'use strict';

const xigniteToken='1F4F497EBF774C7E9E93B1B701C01B3E';
const apilayerToken='78d38860da7ba6a3274973b917508190';
const ALL_PAIRS_NAMES = ['USD/EUR', 'EUR/USD', 'BTC/USD', 'BTC/EUR'];
const redisKeysUtil = require('../server/utils/redis-keys-names.util');
let PAIRS_RESOURCES_COUNT = {};
let PAIRS_REDIS_KEYS = {};
const RESOURCES = [
  {
    // this resource does not provide possibility to fetch data for multiply base currencies
    // so, we will call it twice
    url: 'http://api.fixer.io/latest?base=USD&symbols=EUR',
    interval: 1000 * 30, // call this API every 30 seconds
    formatterName: 'fixer',
    name: 'fixer',
    returnedPairs: ['USD/EUR'],
    ttl: 60 // Time To Live for saved data, in seconds
  },
  {
    url: 'http://api.fixer.io/latest?base=EUR&symbols=USD',
    interval: 1000 * 30,
    formatterName: 'fixer',
    name: 'fixer1',
    returnedPairs: ['EUR/USD'],
    ttl: 60
  },
  {
    url: 'http://apilayer.net/api/live?access_key=' + apilayerToken + '&source=USD&currencies=EUR',
    interval: 1000 * 30,
    formatterName: 'apilayer',
    name: 'apilayer',
    returnedPairs: ['USD/EUR'],
    ttl: 60
  },
  {
    url: 'http://api.coindesk.com/v1/bpi/currentprice.json',
    interval: 1000 * 30,
    formatterName: 'coindesk',
    name: 'coindesk',
    returnedPairs: ['BTC/USD', 'BTC/EUR'],
    ttl: 60
  },
  {
    url: [
      'http://globalcurrencies.xignite.com/xGlobalCurrencies.json/GetRealTimeRates?_token=',
      xigniteToken,
      '&Symbols=BTCUSD,BTCEUR'].join(''),
    interval: 1000 * 30,
    formatterName: 'xignite',
    name: 'xignite',
    returnedPairs: ['EUR/USD', 'USD/EUR', 'BTC/USD', 'BTC/EUR'],
    ttl: 60
  },
  {
    url: 'https://api.cryptonator.com/api/ticker/btc-usd',
    interval: 1000 * 30,
    formatterName: 'cryptonator',
    name: 'cryptonator',
    returnedPairs: ['BTC/USD'],
    ttl: 60 * 100
  },
  {
    url: 'https://api.cryptonator.com/api/ticker/btc-eur',
    interval: 1000 * 30,
    formatterName: 'cryptonator',
    name: 'cryptonator1',
    returnedPairs: ['BTC/EUR'],
    ttl: 60
  }
];

// init resources counters
ALL_PAIRS_NAMES.forEach((name) => {
  PAIRS_RESOURCES_COUNT[name] = 0;
  PAIRS_REDIS_KEYS[name] = [];
});

// count used resources for each pair
RESOURCES.forEach((resource) => {
  resource.returnedPairs.forEach((name) => {
    PAIRS_RESOURCES_COUNT[name] +=1;
    PAIRS_REDIS_KEYS[name].push(redisKeysUtil.getKeyPairName(name, resource.name));
  })
});

// RESOURCES and CHECK_EXPIRED_RATES_INTERVAL should be configured depends on how often API for updating rates will be called
module.exports = {
  RESOURCES: RESOURCES,
  TTL_OF_RATE: 1000 * 60, // 60 seconds, TTL for Redis keys with info about currency pair rates, will be used if ttl for resource is not defined
  ALL_PAIR_NAMES: ALL_PAIRS_NAMES,
  PAIRS_RESOURCES_COUNT: PAIRS_RESOURCES_COUNT,
  PAIRS_REDIS_KEYS: PAIRS_REDIS_KEYS
};
