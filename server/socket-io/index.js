'use strict';

const socketIO = require('socket.io');
const notifier = require('./users-notifier');
const tickerChannelRoomName = require('../../constants/socket-io.const').TICKER_CH_ROOM_NAME;

// for having possibility to have several io services(servers on multiply cores/machines)
// need to use some external adapter
// for storing sockets, it can be redis
function IOServer(server) {
  let self = this;
  self.server = socketIO(server);
  self.server.on('connection', function (socket) {
    socket.join(tickerChannelRoomName);

    socket.on('disconnect', function () {
      socket.leaveAll();
      notifier.onUserDisconnect();
    });

    notifier.onUserConnect(socket);
  });
}

module.exports = IOServer;
