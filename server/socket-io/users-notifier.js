'use strict';

const CurrencyModel = require('../models').Currency;
const notifyInterval = 1000 * 10;
const tickerChannelRoomName = require('../../constants/socket-io.const').TICKER_CH_ROOM_NAME;
let socketSendRatesTimer;

function onUserConnect(socket) {
  _onConnectNotify(socket);

  // init notifying if required
  if (!socketSendRatesTimer) {
    socketSendRatesTimer = setInterval(() => {
      _sendRatesByInterval();
    }, notifyInterval);
  }
}

function onUserDisconnect() {
  const ioServerInstance = require('./../../server').get('io-server');
  const tickerChannelRoom = ioServerInstance.sockets.in(tickerChannelRoomName);
  let tickerRoom = tickerChannelRoom.adapter.rooms[tickerChannelRoomName];

  // stop calculating and sending rates to the appropriate rate because there are no users
  if (!tickerRoom || !tickerRoom.length) {
    clearInterval(socketSendRatesTimer);
    socketSendRatesTimer = null;
  }
}

function _sendRatesByInterval() {
  const ioServerInstance = require('./../../server').get('io-server');

  CurrencyModel.getAllRates((err, rates) => {
    if (err) {
      return ioServerInstance.sockets.in(tickerChannelRoomName).emit('rates', {
        type: 'error',
        data: err
      });
    }

    ioServerInstance.sockets.in(tickerChannelRoomName).emit('rates', {
      type: 'data',
      data: rates
    });
  });
}

function _onConnectNotify(socket) {
  return CurrencyModel.getAllRates((err, rates) => {
    if (err) {
      return socket.emit('rates', {
        type: 'error',
        data: err
      });
    }

    socket.emit('rates', {
      type: 'data',
      data: rates
    });
  });
}

module.exports = {
  onUserConnect: onUserConnect,
  onUserDisconnect: onUserDisconnect
};
