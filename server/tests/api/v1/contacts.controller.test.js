'use strict';

var app = require('../../../../server');
var request = require('supertest');
var Q = require('q');
var chai = require('chai');
var expect = chai.expect;

const db = require('../../../models');

describe('Run Contacts API Tests ', function () {
  let newContact = {
    lastName: 'last',
    firstName: 'first',
    birthDate: new Date(),
    phoneNumbers: ['380996698']
  };
  let newContactForAPI = {
    lastName: 'last_api',
    firstName: 'first_api',
    birthDate: new Date(),
    phoneNumbers: ['3809966981']
  };

  let updateContactData = {
    lastName: 'last_upd1',
    firstName: 'first_upd',
    phoneNumbers: ['38099669823']
  };

  let defaultContact;

  function createContact(newContact) {
    return db.Contact.create(newContact)
      .then(function (contact) {
        return {contact: contact};
      }, function (err) {
        throw err;
      });
  }

  before(function (done) {
    Q.all([
      db.Contact.remove({})
    ]).then(function () {
      createContact(newContact).then(function (result) {
        defaultContact = result.contact;
        done();
      }).catch(function (err) {
        done(err);
      });
    });
  });

  after(function (done) {
    Q.all([
      db.Contact.remove({})
    ]).then(function () {
      done();
    })
      .catch(function (err) {
        done(err);
      });
  });

  //----- CRUD OPERATIONS

  describe('POST /api/v1/contacts', function () {

    it('should respond with correct JSON object', function (done) {
      request(app)
        .post('/api/v1/contacts')
        .send(newContactForAPI)
        .expect(201)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body).to.have.property('_id');
          expect(res.body.lastName).is.equal(newContactForAPI.lastName);
          expect(res.body.firstName).is.equal(newContactForAPI.firstName);
          expect(res.body.phoneNumbers).to.deep.equal(newContactForAPI.phoneNumbers);
          done();
        });
    });
  });

  describe('GET /api/v1/contacts', function () {

    it('should respond with JSON array', function (done) {
      request(app)
        .get('/api/v1/contacts?limit=10&offset=0')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body).to.have.property('docs');
          expect(res.body).to.have.property('limit');
          expect(res.body).to.have.property('offset');
          expect(res.body.docs).to.be.an.instanceof(Array);
          expect(res.body.limit).to.equal(10);
          expect(res.body.offset).to.equal(0);
          done();
        });
    });
  });

  describe('GET /api/v1/contacts/:id', function () {

    it('should respond with JSON object', function (done) {
      request(app)
        .get('/api/v1/contacts/' + defaultContact._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body).to.have.property('_id');
          expect(res.body._id).is.equal(defaultContact._id.toString());
          expect(res.body.lastName).is.equal(defaultContact.lastName);
          expect(res.body.firstName).is.equal(defaultContact.firstName);
          expect(res.body.phoneNumbers).to.have.all.members(defaultContact.phoneNumbers);
          done();
        });
    });
  });

  describe('PUT /api/v1/contacts/:id', function () {

    it('should respond with JSON object', function (done) {
      request(app)
        .put('/api/v1/contacts/' + defaultContact._id)
        .send(updateContactData)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body).to.have.property('_id');
          expect(res.body._id).is.equal(defaultContact._id.toString());
          expect(res.body.lastName).is.equal(updateContactData.lastName);
          expect(res.body.firstName).is.equal(updateContactData.firstName);
          expect(res.body.phoneNumbers).to.have.all.members(updateContactData.phoneNumbers);
          done();
        });
    });
  });

  describe('DELETE /api/v1/contacts/:id', function () {

    it('should respond with JSON Object and 204', function (done) {
      request(app)
        .del('/api/v1/contacts/' + defaultContact._id)
        .expect(204)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          done();
        });
    });
  });

});
