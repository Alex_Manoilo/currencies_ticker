'use strict';

var app = require('../../../../server');
var callConstants = require('../../../../constants/calls.const');
var request = require('supertest');
var Q = require('q');
var chai = require('chai');
var expect = chai.expect;

const db = require('../../../models');

describe('Run Calls API Tests ', function () {
  let newContact = {
    lastName: 'last',
    firstName: 'first',
    birthDate: new Date(),
    phoneNumbers: ['380996698']
  };
  let newCall = {
    type: callConstants.TYPES_MAP.INCOMING,
    duration: 100,
    startDate: new Date()
  };
  let newCallForAPI = {
    type: callConstants.TYPES_MAP.OUTCOMING,
    duration: 43,
    startDate: new Date()
  };

  let defaultContact;
  let defaultCall;

  function createContactWithCall(newContact) {
    return db.Contact.create(newContact)
      .then(function (createdContact) {
        defaultContact = createdContact;
        newCall.contact = createdContact._id;
        newCall.number = createdContact.phoneNumbers[0];
        return createCall(newCall);
      }, function (err) {
        throw err;
      });
  }

  function createCall(newCall) {
    return db.Call.create(newCall)
      .then(function (createdCall) {
        return {call: createdCall};
      }, function (err) {
        throw err;
      });
  }

  before(function (done) {
    Q.all([
      db.Call.remove({})
    ]).then(function () {
      createContactWithCall(newContact).then(function (result) {
        defaultCall = result.call;
        newCallForAPI.contact = result.call.contact;
        newCallForAPI.number = result.call.number;
        done();
      })
        .catch(function (err) {
          done(err);
        });
    });
  });

  after(function (done) {
    Q.all([
      db.Contact.remove({}),
      db.Call.remove({})
    ]).then(function () {
      done();
    })
      .catch(function (err) {
        done(err);
      });
  });

  //----- CRUD OPERATIONS
  describe('POST /api/v1/calls', function () {

    it('should respond with correct JSON object', function (done) {
      request(app)
        .post('/api/v1/calls')
        .send(newCallForAPI)
        .expect(201)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body).to.have.property('_id');
          expect(res.body.contact).is.equal(newCallForAPI.contact.toString());
          expect(res.body.type).is.equal(newCallForAPI.type);
          expect(res.body.duration).is.equal(newCallForAPI.duration);
          done();
        });
    });
  });

  describe('GET /api/v1/calls', function () {

    it('should respond with JSON array', function (done) {
      request(app)
        .get('/api/v1/calls?limit=10&offset=0')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body).to.have.property('docs');
          expect(res.body).to.have.property('limit');
          expect(res.body).to.have.property('offset');
          expect(res.body.docs).to.be.an.instanceof(Array);
          expect(res.body.limit).to.equal(10);
          expect(res.body.offset).to.equal(0);
          done();
        });
    });
  });

  describe('GET /api/v1/calls/:id', function () {

    it('should respond with JSON object', function (done) {
      request(app)
        .get('/api/v1/calls/' + defaultCall._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body._id).is.equal(defaultCall._id.toString());
          expect(res.body).to.have.property('contact');
          expect(res.body.contact).to.have.property('_id');
          expect(res.body.contact._id).is.equal(defaultCall.contact.toString());
          expect(res.body.type).is.equal(defaultCall.type);
          expect(res.body.duration).is.equal(defaultCall.duration);
          done();
        });
    });
  });

  describe('GET /api/v1/contacts/:id/calls', function () {

    it('should respond with JSON object', function (done) {
      request(app)
        .get('/api/v1/contacts/' + defaultContact._id + '/calls')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          expect(res.body).to.have.property('docs');
          expect(res.body.docs).to.have.length(2);
          expect(res.body.docs[0]).to.have.property('_id');
          expect(res.body.docs[0]).to.have.property('type');
          expect(res.body.docs[0]).to.have.property('duration');
          expect(res.body.docs[0]).to.have.property('startDate');
          expect(res.body.docs[0]).to.have.property('contact');
          expect(res.body.docs[0]).to.have.property('number');
          done();
        });
    });
  });

  describe('DELETE /api/v1/calls/:id', function () {

    it('should respond with JSON Object and 204', function (done) {
      request(app)
        .del('/api/v1/calls/' + defaultCall._id)
        .expect(204)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.an.instanceof(Object);
          done();
        });
    });
  });

});
