'use strict';

const redis = require("redis");
const client = redis.createClient();

client.on("error", function (err) {
  console.error("Error connect to the redis", err);
});

module.exports = client;


