'use strict';

const request = require('request');
const currenciesConstants = require('../../constants/currencies.const');
const API_RESOURCES_LIST = currenciesConstants.RESOURCES;
const resourceFormatter = require('../utils/formatter.util').currenciesResponseFormatter;
const CurrencyModel = require('../models').Currency;
let requestsStore = {};

function init() {
  _startGrabbing();
}

function _startGrabbing() {
  API_RESOURCES_LIST.forEach((resource) => {
    _grabRatesFromResource(resource);
    setInterval(()=> {
      _grabRatesFromResource(resource);
    }, resource.interval);
  });
}

function _grabRatesFromResource(resource) {
  if (requestsStore[resource.name] && !requestsStore[resource.name]._ended) {
    requestsStore[resource.name].abort();
  }

  requestsStore[resource.name] = request({
    method: 'GET',
    uri: resource.url
  }, function (error, response, body) {
    let responseBody;

    if (error || (response.statusCode !== 200 && response.statusCode !== 304) ) {
      // here we can add calling method for increasing interval for calling appropriate API
      // and increase that interval depends on number of failures
      // and in the case when all is fine we can restore basic interval for calling the service
      return console.error('request currency resource error, url: ', resource.url, '\nerr: ', error, 'body: ', body);
    }

    try {
      responseBody = JSON.parse(body);
    } catch (err) {
      return console.error("Can't parse response from currency service, err: ", err, 'body: ', body);
    }

    let formattedResponse = resourceFormatter[resource.formatterName](responseBody);

    if (formattedResponse && formattedResponse.length) {
      CurrencyModel.savePairsRate(formattedResponse, resource);
    }
  });
}

module.exports = {
  init: init
};