'use strict';

let chalk = require('chalk'),
  _ = require('lodash');

/**
 *
 * @param {Object|Error} err
 * @param {ServerResponse} res
 * @returns {ServerResponse}
 */
module.exports = (err, res) => {
  err = _.isObject(err) ? err : {};

  let toPrint = err instanceof Error ? err : JSON.stringify(err, undefined, 2);
  console.error(chalk.red(toPrint, toPrint.stack ? toPrint.stack : ''));

  let status = err.status || 500;

  return res.status(status).json({
    message: err.message,
    errors: err.errors
  });
};
