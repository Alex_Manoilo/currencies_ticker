'use strict';

const _ = require('lodash');
const SUPPORTED_CURRENCY_PAIRS = require('../../constants/currencies.const').ALL_PAIR_NAMES;

class currenciesResponseFormatter {

  static fixer(response) {
    if (!response.rates || Object.keys(response.rates).length === 0) {
      return console.error('fixer error response: ', response);
    }

    let baseName = response.base;
    let currenciesData = [];

    _.forEach(response.rates, (currencyRate, currencyName) => {
      let pairName = baseName + '/' + currencyName;

      // save only required pairs, some resources hard to configure exactly required data
      if (SUPPORTED_CURRENCY_PAIRS.indexOf(pairName) !== -1) {
        currenciesData.push({
          pairName: pairName,
          value: parseFloat(currencyRate)
        });
      }
    });

    return currenciesData;
  }

  static coindesk(response) {
    if (!response.bpi || Object.keys(response.bpi).length === 0) {
      return console.error('coindesk error response: ', response);
    }

    let baseName = 'BTC';
    let currenciesData = [];

    _.forEach(response.bpi, (currencyRateData, currencyName) => {
      let pairName = baseName + '/' + currencyName;

      if (SUPPORTED_CURRENCY_PAIRS.indexOf(pairName) !== -1) {
        currenciesData.push({
          pairName: pairName,
          value: parseFloat(currencyRateData.rate_float)
        });
      }
    });

    return currenciesData;
  }

  static apilayer(response) {
    if (!response.success) {
      return console.error('apilayer error response: ', response);
    }

    let baseName = response.source;
    let currenciesData = [];

    _.forEach(response.quotes, (currencyRateData, currencyName) => {
      let pairName = baseName + '/' + currencyName.substr(baseName.length);

      if (SUPPORTED_CURRENCY_PAIRS.indexOf(pairName) !== -1) {
        currenciesData.push({
          pairName: pairName,
          value: parseFloat(currencyRateData)
        });
      }
    });

    return currenciesData;
  }

  static xignite(response) {
    if (response.Outcome !== 'Success') {
      return console.error('xignite error response: ', response[0].Message);
    }

    let currenciesData = [];

    _.forEach(response, (currencyRateData) => {
      let pairName = currencyRateData.BaseCurrency + '/' + currencyRateData.QuoteCurrency;

      if (SUPPORTED_CURRENCY_PAIRS.indexOf(pairName) !== -1) {
        currenciesData.push({
          pairName: pairName,
          value: parseFloat(currencyRateData.Mid)
        });
      }
    });

    return currenciesData;
  }

  static cryptonator(response) {
    if (!response.success) {
      return console.error('cryptonator error response: ', response.error);
    }

    let pairName = response.ticker.base + '/' + response.ticker.target;
    let currenciesData = [];

    if (SUPPORTED_CURRENCY_PAIRS.indexOf(pairName) !== -1) {
      currenciesData.push({
        pairName: pairName,
        value: parseFloat(response.ticker.price)
      });
    }

    return currenciesData;
  }
}

module.exports = {
  currenciesResponseFormatter: currenciesResponseFormatter
};
