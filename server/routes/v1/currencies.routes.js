'use strict';

const express = require('express');
const currenciesCtrl = require('../../controllers/v1/currencies.controller');
const noCacheMiddleware = require('../../middlewares/nocache-middleware');

module.exports = function (app) {
  let router = express.Router();

  router.get('/', noCacheMiddleware, currenciesCtrl.getAll);

  app.use('/api/v1/currencies', router);
};
