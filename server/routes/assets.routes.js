'use strict';

// in production  it will be served via nginx or some static server
const express = require('express');
const staticCtrl = require('../controllers/static.controller');

module.exports = function (app) {
  let router = express.Router();

  router.get('/*', staticCtrl.assets);
  app.use('/assets', router);
};
