'use strict';

const CurrencyModel = require('../../models').Currency;
const errorResponse = require('../../responses/v1/server_error.response');

exports.getAll = function (req, res) {
  CurrencyModel.getAllRates((err, rates) => {
    if (err) {
      return errorResponse(err, res)
    }

    res.status(200).json(rates)
  });
};
