'use strict';

const fs = require('fs');

function renderStaticHtmlFile(path) {
  return function (req, res, next) {
    fs.createReadStream(__dirname + '/../../frontend/' + path)
      .pipe(res);
  };
}

function returnAssets(req, res, next) {
  let path = req.params[0];
  let stream = fs.createReadStream(__dirname + '/../../frontend/assets/' + path);

  stream.on('open', function () {
    stream.pipe(res);
  });

  stream.on('error', function(err) {
    res.end(err.message);
  });
}

module.exports = {
  index: renderStaticHtmlFile('index.html'),
  assets: returnAssets
};
