'use strict';

const glob = require('glob');
const path = require('path');

const db = {};
const files = glob.sync('**/!(index).js', {cwd: path.dirname(module.filename)});

files.forEach((file) => {
  let modelData = require('./' + file);
  let model = modelData.model;
  let modelName = modelData.name;
  db[modelName] = model;
});

module.exports = db;
