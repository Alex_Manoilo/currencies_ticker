'use strict';

const async = require('async');
const redisClient = require('../services/redis-client.service');
const redisKeysUtil = require('../utils/redis-keys-names.util');
const currenciesConstants = require('../../constants/currencies.const');
const TTL_OF_RATE = currenciesConstants.TTL_OF_RATE;
const ALL_CURRENCIES_PAIR_NAMES = currenciesConstants.ALL_PAIR_NAMES;

class CurrencyModel {
  constructor() {}

  static savePairRate(options) {
    let keyName = redisKeysUtil.getKeyPairName(options.pairName, options.resource.name);
    let ttl = options.resource.ttl || TTL_OF_RATE;

    redisClient.set(keyName, options.value);
    redisClient.expire(keyName, ttl);
  }

  static savePairsRate(data, resource) {
    data.forEach((pairData) => {
      this.savePairRate({
        resource: resource,
        pairName: pairData.pairName,
        value: pairData.value
      })
    });
  }

  static getPairRate(pairName, cb) {
    let pairRedisKeys = currenciesConstants.PAIRS_REDIS_KEYS[pairName];
    let minRate;

    redisClient.MGET(pairRedisKeys, (err, pairValues) => {
      if (err) {
        return cb(err);
      }

      let actualResourcesValues = pairValues.filter((val) => val);
      let usedResourcesCount = actualResourcesValues.length;

      if (!usedResourcesCount) {
        minRate = 'Data is unavailable now';
        console.error(`Data for currencies pair ${pairName} is unavailable!`);
        // here we can implement force calling API for grabbing currencies data
      } else {
        minRate = Math.min.apply(null, actualResourcesValues);
      }

      return cb(null, {
        pair: pairName,
        usedResourcesCount: usedResourcesCount,
        allResourcesCount: currenciesConstants.PAIRS_RESOURCES_COUNT[pairName],
        minRate: minRate
      });
    });
  }

  static getAllRates(cb) {
    async.map(ALL_CURRENCIES_PAIR_NAMES, this.getPairRate, (err, results)=>{
      if (err) {
        return cb(err);
      }

      return cb(null, results);
    });
  }
}

module.exports = {
  model: CurrencyModel,
  name: 'Currency'
};
