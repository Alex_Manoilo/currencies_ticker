'use strict';

$(function(){
  var getRatesViaAPIInterval = 1000 * 30; // every 30 seconds
  var container = $('#ratesContainer');

  /**
   * this function can be used for grabbing rates data via API (it's can be useful for some users and in case of a huge number )
   */
  function initRestCalls() {
    // getRates();
    // setInterval(function () {
    //   getRates();
    // }, getRatesViaAPIInterval)
  }

  function getRates() {
    $.ajax({
      url: "/api/v1/currencies",
      method: 'GET'
    })
        .done(function(data) {
          renderRates(data);
        })
        .fail(function() {
          console.error("Error during getting rates!");
          renderError();
        });
  }

  function initSocketIo() {
    var socket = io();

    socket.on('connect', function(msg){
      console.log('socket connected: ', msg);
    });

    socket.on('disconnect', function(msg){
      console.log('socket disconnected: ', msg);
      renderError('Socket disconnected.');
    });

    socket.on('rates', function(msg){
      switch(msg.type) {
        case 'data':
          renderRates(msg.data);
          break;
        case 'error':
          renderError();
          break;
        default:
          break;
      }
    });
  }

  function renderRates(data) {
    var rates = data.map(function(rateData) {
      return ['<li>', rateData.pair,': ', rateData.minRate, '</li>'].join('')
    });
    var usedResourcesInfo = data.map(function(rateData) {
      return [
        '<li>', rateData.pair,' (', rateData.usedResourcesCount, ' of ', rateData.allResourcesCount, ')</li>'
      ].join('')
    });
    container.html(rates.concat(usedResourcesInfo));
  }

  function renderError(msg) {
    msg = msg || '';
    container.html('<li> Error during getting rates from the server! ' + msg + ' </li>');
  }

  initSocketIo();
});
