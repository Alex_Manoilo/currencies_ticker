## Prerequisites
Make sure you have installed all these prerequisites on your development machine.
* Node.js - [Download & Install Node.js](http://www.nodejs.org/download/) and the npm package manager, if you encounter any problems, you can also use this [GitHub Gist](https://gist.github.com/isaacs/579814) to install Node.js.

## Quick Install
    
```bash
$ npm install
# then check console messages and follow by printed link to setup password for the first user
```

## Running Your Application
After the install process is over, you'll be able to run your application

```bash
$ npm start
```

Your application should run on the 3000 port so in your browser just go to [http://localhost:3000/index.html](http://localhost:3000/index.html)