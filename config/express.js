'use strict';

const fs = require('fs'),
  express = require('express'),
  morgan = require('morgan'),
  glob = require('glob'),
  path = require('path'),
  staticCtrl = require('../server/controllers/static.controller');

module.exports = function () {
  // Initialize express app
  const app = express();

  // Environment dependent middleware
  if (process.env.NODE_ENV === 'development') {
    // Enable logger (morgan)
    app.use(morgan('dev'));
  }

  // at first use app routes
  glob.sync('./server/routes/**/*.js').forEach(function (routePath) {
    require(path.resolve(routePath))(app);
  });

  app.get(['/app', '/index', '/index.html'], staticCtrl.index);

  app.use(function (err, req, res, next) {
    if (!err) return next();

    console.error(err.stack);

    res.status(500)
      .send('500', {error: err.stack});
  });

  return app;
};
