'use strict';

const _ = require('lodash');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

/**
 * Load app configurations
 */
module.exports = _.extend(
  require('./env/' + process.env.NODE_ENV) || {}
);
