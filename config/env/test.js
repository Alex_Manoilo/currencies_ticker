'use strict';

module.exports = {
  httpPort: process.env.HTTP_PORT || 3030
};