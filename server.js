/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */
'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.on('uncaughtException', err => console.error(err.stack));
console.log('Application running in', process.env.NODE_ENV, 'mode');

const http = require('http');
const init = require('./config/init')();
const config = require('./config/config');
const IOServer = require('./server/socket-io');

// Init the express application and start it
const app = require('./config/express')();
const server = http.createServer(app).listen(config.httpPort);
const currenciesGrabber = require('./server/services/currencies-grabber.service');

// start getting and saving data from currency resources
// if we will have a lot of clients we can move getting and updating rates data to the separate service
// and the Redis store will be the common
currenciesGrabber.init();

const ioServer = new IOServer(server);
app.set('io-server', ioServer.server);

console.log('Application has started on port: ', config.httpPort);

module.exports = app;
